<!DOCTYPE html>
<html lang="en">

<head>
        <title>B-spaces</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/p-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">
            <form id="loginform" class="form-vertical" action="index.php">
				 <div class="control-group normal_text"> <h3><img src="img/logo.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" placeholder="Username" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="Password" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                  <button  data-toggle="modal" data-target="#modal_otp" type="button" class="btn btn-primary">Lost password?</button>
                    <span class="pull-right"><a type="submit" href="pages/main.php" class="btn btn-success" /> Login</a></span>
                </div>
                <div data-toggle="modal" data-target="#modal_signup">
                  <button type="button" class="btn btn-secondary"style="margin-left:125px; width:190px">Sign Up
                    <span aria-hidden="true"></span></button></div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>

                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>

                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Reecover</a></span>
                </div>
            </form>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/matrix.login.js"></script>
    </body>

    <!--modal-signup-->
    <div class="modal fade" id="modal_signup">
      <div class="modal-dialog"role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">
              <span aria-hidden="true">x</span>
              </button>
            <h2>Sign Up</h2>
            <br></div>
          <div class="modal-body" >
            <form>
              <div class="form-group">
                <label for="name">Enter your Name:</label>
            <input type="text" class="form-control" id="name">
             <br>
             <label for="phno">Enter your Phone Number:</label>
           <input type="text" class="form-control" id="phone">
             <br>
                </form>
                <br>
                <button type="button" onclick="showmPinModal()" class="btn btn-success">Send OTP</button>

    </div></div>
    </div></div>
    </div></div>
    </div>
    </div>
    </div>
    <!--end-->

    <!--otp-modal-->
    <div class="modal fade" id="modal_otp">
      <div class="modal-dialog"role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">
              <span aria-hidden="true">x</span>
              <span class="sr-only">Close</span>
              </button>
            <h3>B-spaces Password Assistance</h3>
            <br></div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="no">Enter your Phone Number</label>
            <input type="number" class="form-control" id="otp-name"><br>
            <button onclick="func33()" type="button"class="btn btn-success">Send OTP</button>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--end-->
<!--otp-sent-modal-->
<div class="modal fade" id="modal_otp_1">
  <div class="modal-dialog"role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
            <span aria-hidden="true"></span>
            <span class="sr-only">x</span>
          </button>
          <h4>We have sent you an OTP</h4></div>
          <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="no">Enter OTP</label>
          <input type="number" class="form-control" id="otp-1"><br>
          <button onclick="showpmodal()" type="button"class="btn btn-success">Go</button></form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end-->

<!--pin-modal-->
<div class="modal fade" id="modal_pin">
  <div class="modal-dialog"role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
            <span aria-hidden="true">x</span>
            <span class="sr-only"></span>
          </button>
          <h4> Set your 4 Digit Login Pin</h4></div>
          <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="no">Enter OTP</label>
          <input type="number" class="form-control" id="otp-3"><br>
          <label for="no">Set your Pin</label>
            <input type="number" class="form-control" id="pin-1"><br>
            <label for="no">Confirm Pin</label>
              <input type="number" class="form-control" id="pin-2"><br>
              <button type="button"class="btn btn-success">Submit</button>
            </div>
        </div>
      </div>
    </div>
  </div>


<!--pin-modal-->
<div class="modal fade" id="modal_pin">
  <div class="modal-dialog"role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">x</span>
          </button>
        <h3>Set your 4 digit Login Pin</h3>
        <br></div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="otpn">Enter the OTP</label>
        <input type="text" class="form-control" id="otp-name">
         <br>
         <label for="pin">Enter the Pin</label>
         <input type="text" class="form-control" id="pin-1">
         <br>
          <label for="pinc">Confirm the Pin</label>
           <input type="text" class="form-control" id="pin-2">
           <br>
            </form>
            <br>
            <button type="button" class="btn btn-success">Update</button>
          </div></div></div></div></div>
<!--end-->


    <script src="../js/excanvas.min.js"></script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.ui.custom.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.flot.min.js"></script>
    <script src="../js/jquery.flot.resize.min.js"></script>
    <script src="../js/jquery.peity.min.js"></script>
    <script src="../js/matrix.js"></script>
    <script src="../js/fullcalendar.min.js"></script>
    <script src="../js/matrix.calendar.js"></script>
    <script src="../js/matrix.chat.js"></script>
    <script src="../js/jquery.validate.js"></script>
    <script src="../js/jquery.wizard.js"></script>
    <script src="../js/jquery.uniform.js"></script>
    <script src="../js/matrix.popover.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.flot.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/login.js"></script>
    <script>
      function showModal(){
        $('#modal_signup').modal('show');
      };
    </script>
    <script>
      function showModal(){
        $('#modal_otp').modal('show');
      };
    </script>

    <script>
      function showModal(){
        $('#modal_pin').modal('show');
      };
    </script>

</html>
