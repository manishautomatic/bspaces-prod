<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="../css/fullcalendar.css" />
<link rel="stylesheet" href="../css/matrix-style.css" />
<link rel="stylesheet" href="../css/matrix-media.css" />
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body style="overflow:hidden; width:100%; margin-bottom:5%;">

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html">Matrix Admin</a></h1>
</div>
<!--close-Header-part-->


<!--close-top-serch-->

<!--sidebar-menu-->

<div id="sidebar"><a href="#" class="visible-phone">
  <ul>
    <li><a href="main.php"><i class="icon icon-home"></i> <span>Home</span></a> </li>
    <li><a data-toggle="modal" href="#modal_new_query" ><i class="icon icon-signal"></i> <span>New Query</span></a> </li>
    <li><a data-toggle="modal" href="#modal_profile"><i class="icon icon-inbox"></i> <span>Profile</span></a> </li>
    <li><a href="subscriptions.php"><i class="icon icon-th"></i> <span>My Subscription</span></a></li></li>
    <li><a href="../login.php"><i class="icon icon-th"></i> <span>Logout</span></a></li></li>

    <p style="text-align:center" >©2017 B-spaces</p>
  </ul>

  <br>


<!--
  <ul style="vertical align:bottom">
    <li style="margin-left:60px">©2017 B-spaces</li>
  </ul>
-->
</div>
<!--end sidebar-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="indexmain.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>

<!-- start container -->
  <div class="container" stlye="display: flex;  height: calc(100vh - 100px); position: relative; width: 100%; margin-bottom:10cm";>
    <div class="col-md-12" style="overflow: auto; height: auto; -webkit-overflow-scrolling: touch; -ms-overflow-style: none; margin-bottom:100px">
        <!-- start of list of queries -->
        <div class="col-md-4" style="overflow: auto; height: 620px; padding: .5rem; -webkit-overflow-scrolling: touch; -ms-overflow-style: none; margin-bottom:100px">
          <div class="panel panel-default">
            <div class="panel body" style="overflow-y:600px; height:auto;">

            <ol style="list-style-type: none; ">
              <li onclick="showResponses()" style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
              <li style="padding:20px"><b>Date:June 8, 2017</b><br>Looking for 10,00 sqft for rent in Industrial Area Chandigarh</li>
            </ol>
          </div>
        </div></div>
        <!-- end of list of queries -->

        <!-- start of list of responses -->
        <div class="col-md-8" style="width: 60%; display:none" id="listing_results_display_container"  >
          <div class="col-mod-12" id="right" >
            <div class="col-mod-6">
              <div class="panel-group">
                <div class="panel panel-default">
              <table class="table" width="2020"; style="background:#FFFFFF">
                  <tr>
                    <td align="left">June 8, 2017</td>
                  </tr>
                <tbody>
                  <tr>
                    <td><b>Location:</b></td>
                    <td>Industrial Area Phase 1, Chandigarh</td>
                  </tr>
                  <tr>
                    <td><b>Area:</b></td>
                    <td>10,000 Sq Ft (commercial)</td>
                  </tr>
                  <tr>
                    <td><b>I want to:</b></td>
                    <td>Lease</td>
                  </tr>
                </tbody>
                </table>
      </div></div></div>
        <div  class="col-mod-6" style="overflow: auto; height: 462px; padding: .5rem; -webkit-overflow-scrolling: touch; -ms-overflow-style: none; ">
          <div class="panel-group">
            <div class="panel panel-default">
              <div class="panel-body" style="background:#FFFFFF; overflow-y:600px; height:3000px; ">
              <ol style="list-style-type: none; -webkit-padding-start:0;">
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>
                <li onclick="showListingDetailModal()" style="padding:20px 250px; border:2px solid #999999; border-radius:5px"><b>INR. 2,40,000</b><br>Plot No 143A<br>10,000 Sq.Ft.<br>(Free Hold)</li>

              </ol>
        </div>
        <!-- end of list of responses -->
    </div>
    <!-- end of main c12 -->
  </div>
  <!-- end container -->
</div>
  <!-- modal-new-query-->
<div class="modal fade" id="modal_new_query">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">X</span>
          <span class="sr-only">Close</span>
          </button>
        <h2>New Query</h2>
        </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
          <label for="Region">Select your Region:</label>
       <select class="form-control" id="region">
         <option>Chandigarh</option>
         <option>Panchkula</option>
         <option>Mohali</option>
       </select>
       <br>
       <label for="area">Select your Area:</label>
       <select class="form-control" id="area">
         <option>sec 17</option>
         <option>sec 10</option>
         <option>sec 12</option>
         <option>sec 15</option>
       </select>
       <br>
       <label for="desired-area">Desired Area type</label>
       <select class="form-control" id="desired-area">
         <option>retail</option>
         <option>residential</option>
         <option>warehouse</option>
       </select>
       <br>
       <label for="int-area">What are you interested in:</label>
       <select class="form-control" id="int-area">
         <option>lease</option>
         <option>rent</option>
       </select>
       <br>
       <label for="usr">Area Required:</label>
     <input type="text" class="form-control" id="usr">
            </form>
            <br>
            <button type="button" class="btn btn-success">Submit</button>
</div>
</div>
</div></div>
</div>
</div>
</div>
<!-- end-modal-new-query-->


<!-- modal-profile-->

<div class="modal fade" id="modal_profile">
<div class="modal-dialog"role="document">
  <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">X</span>
          <span class="sr-only">Close</span>
          </button>
        <h2>Profile</h2></div>
        <div class="modal-body">
      <form>
        <div class="form-group">
          <label for="name-profile">Name:</label>
        <input type="text" class="form-control" id="name-profile">
        <br>
        <label for="email-profile">Email:</label>
      <input type="text" class="form-control" id="email-profile">
      <br>
      <label for="phone-profile">Phone:</label><br>
    <input type="text" class="form-control" id="phone-profile"><br>
    <button type="button" class="btn btn-success"> Update </button>
    <br><br><br>
    <label for="pass-profile">Password:</label>
    <input type="text" class="form-control" id="pass-profile">
    <br>
    <label for="nwpas-profile">New Password:</label>
      <input type="text" class="form-control" id="nwpas-profile">
    <br>
    <label for="cnp-profile">Confirm Password:</label>
    <input type="text" class="form-control" id="cnp-profile">
    <br>
    <button type="button" class="btn btn-success"> Update </button>
  </div></div></div></div></div></div>
  <!-- end-modal-profile-->

<!--li-modal-->
<div class="contaier">
<div class="modal fade" id="li-modal">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-body">
      <p>asdadad</p>
        </div></div></div></div></div>



<!--end-li-modal-->

<!--modal-pop-up-->
<div class="modal fade" id="pop-up">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">x</span>
          <span class="sr-only">Close</span>
          </button>
        <h5 class="modal-title">Listing Details</h5>
      </div>
      <div class="modal-body">
        <img src="img/gallery/imgbox1.jpg" class="rounded mx-auto d-block" alt="listing image">
        <table class="table" width="2020">
          <tr>
            <td><b>Location:</b></td>
            <td>Plot No 143-A, Purv Marg Industrial Area Phase 1 Chandigarh</td>
          </tr>
          <tr>
            <td><b>Description</b></td>
            <td>This is the place where we can add some descirption associated with this listing. This will be added at the time listing is posted.</td>
          </tr>
          <tr>
            <td><b>Carpet Area</b></td>
            <td>10,000 Sq Ft</td>
          </tr>
          <tr>
            <td><b>Price</b></td>
            <td>Rs24,000,000</td>
          </tr>
          <tr>
            <td><b>Availability Type</b></td>
            <td>For Lease</td>
          </tr>
          <tr>
            <td><b>Legal Disputes</b></td>
            <td>None</td>
          </tr>
          <tr>
            <td><b>Contact Person</b></td>
            <td>Mr Manish Kumar</td>
          </tr>
          <tr>
            <td><b>Contact Email</b></td>
            <td>abc@abc.com</td>
          </tr>
          <tr>
            <td><b>Contact Phone</b></td>
            <td>0000000000</td>
          </tr>
          <tr>
            <td><b>Website</b></td>
            <td>abc.com</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<!--end-pop-ip-->

<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; B-Spaces</div>
</div>
<!--end-Footer-part-->




<script src="../js/excanvas.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.ui.custom.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.flot.min.js"></script>
<script src="../js/jquery.flot.resize.min.js"></script>
<script src="../js/jquery.peity.min.js"></script>
<script src="../js/matrix.js"></script>
<script src="../js/fullcalendar.min.js"></script>
<script src="../js/matrix.calendar.js"></script>
<script src="../js/matrix.chat.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/jquery.wizard.js"></script>
<script src="../js/jquery.uniform.js"></script>
<script src="../js/matrix.popover.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/main.js"></script>


<!--<script src="js/test-1.js"></script>-->
<script type="text/javascript">

function showAlert(){
  alert('sasas');
}
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {

          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();
          }
          // else, send page to designated URL
          else {
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
$(document).ready(function(){

});


function showListingDetailModal(){
  $('#pop-up').modal('show');
}

</script>
</body>
</html>
