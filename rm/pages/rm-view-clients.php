<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="../css/matrix-style.css" />
<link rel="stylesheet" href="../css/matrix-media.css" />
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body style="overflow:hidden; width:100%; margin-bottom:5%;">
<div id="header">
  <h1><a href="dashboard.html"></a>
</div>
<!--Sidebar-->
<div id="sidebar">
  <ul>
    <li><a href="rm-view-listings.php"><i class="icon icon-signal"></i> <span>My Listings</span></a> </li>
    <li><a href="rm-view-clients.php"><i class="icon icon-inbox"></i> <span>My Clients</span></a> </li>
    <li><a href="../rm-login.php"><i class="icon icon-th"></i> <span>Logout</span></a></li></li>
    <p style="text-align:center" >©2017 B-spaces</p>
  </ul>
</div>

<body>

    <div id="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="margin-bottom:0; margin-left:10px">Clients
                      <div class="pull-right" style="margin-right:10px"><a type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#new-client">Add New</a></div>
                    </h1>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="container">
                <div class="col-lg-12" style="padding:0px">
                  <br>  <div class="panel panel-default">
                        </div><br>
                        <div class="row">
                        <div class="col-lg-12">
                                        <div class="panel panel-default">
                                          <div class="panel-body">
                                            <form class="form-inline">
                                              <label for="ex3">Search By: Name / Mobile / Email</label>
                                            <input class="form-control" id="ex3" type="text">
                                            <a type="button" class="btn btn-info"> Search</a>
                                          </form>
                                          </div>
                                        </div>
                                        <div class ="col-lg-6">

                                        </div>
                                      </div>
                                    </div>

                        <br><div class="panel-body">
                          <div> <h2 id="action_status"></h2></div>
                          <br>
                          <div id="clients_data">fetching data please wait..</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <!--Modal_new_client-->
      <div class="modal fade" id="new-client">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style="height:auto; overflow:auto">
            <div class="modal-header">
              <button data-dismiss="modal" class="close" type="button">
                <span aria-hidden="true">x</span>
                <span class="sr-only">Close</span>
                </button>
              <h5 class="modal-title">Add New Client</h5>
            </div>
            <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                  <div class="panel-body" style="width:300px">
                    <label for="ex1">Name</label>
                    <input class="form-control" id="cname" id="cname" type="text"><br>
                    <label for="ex1">Mobile</label>
                    <input class="form-control" id="mobile" id="mobile" type="number"><br>
                    <label for="ex1">Address</label>
                    <input class="form-control" id="address" id="address" type="text"><br>
                    <label for="ex1">Email</label>
                    <input class="form-control" id="email" id="email" type="text"><br>
                    <div >
                      <button type="submit"
                              class="btn btn-md btn-success"
                              id="btnAddClient" onclick="addClient()">ADD</button>
                    </div>

                  </div>
                </div>
              </div>
                <div class="col-lg-1"></div>

            </div>

      <!-- /#page-wrapper -->

  </div>
            </div>
            </div>
          </div>


      <!--End-->

    <script src="../js/excanvas.min.js"></script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.ui.custom.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.flot.min.js"></script>
    <script src="../js/jquery.flot.resize.min.js"></script>
    <script src="../js/jquery.peity.min.js"></script>
    <script src="../js/matrix.js"></script>
    <script src="../js/matrix.chat.js"></script
    <script src="../js/jquery.validate.js"></script>
    <script src="../js/jquery.wizard.js"></script>
    <script src="../js/jquery.uniform.js"></script
    <script src="../js/matrix.popover.js"></script
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.flot.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript
    <script src="../js/metisMenu/metisMenu.min.js"></script>

    <!- DataTables JavaScript -->
    <script src="../js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" >

    <!-- Custom Theme JavaScript -->
    <script src="../js/listings.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    <script src="../js/clients.js"></script>
</body>
</html>
