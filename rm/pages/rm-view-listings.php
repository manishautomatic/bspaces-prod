<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="../css/matrix-style.css" />
<link rel="stylesheet" href="../css/matrix-media.css" />
<link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body style="overflow:hidden; width:100%; margin-bottom:5%;">
<div id="header">
  <h1><a href="dashboard.html"></a>
</div>
<!--Sidebar-->
<div id="sidebar">
  <ul>
    <li><a href="rm-view-listings.php"><i class="icon icon-signal"></i> <span>My Listings</span></a> </li>
    <li><a href="rm-view-clients.php"><i class="icon icon-inbox"></i> <span>My Clients</span></a> </li>
    <li><a href="../rm-login.php"><i class="icon icon-th"></i> <span>Logout</span></a></li></li>
    <p style="text-align:center" >©2017 B-spaces</p>
  </ul>

</div>
<!--End-->

<!--Start Container-->
<div id="content">
  <div class="container" stlye="display: flex;  height: calc(100vh - 100px); position: relative; width: 100%; margin-right:0px; display:none; content:none;">
    <div class="col-md-12" style="margin-bottom:15px; background:#FFFFFF; margin-right:0px; margin-left:85px;  width: 837px">
    <div class="panel-group" style="margin-top:10px">
      <div class="panel panel-default" style:"background:#FFFFFF">
          <button style="margin-left:280px; margin-top:10px; margin-bottom:10px" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#new-lstng" >Add New Listing</button>
            </div>
           <div class="panel-group">
       </div>
     </div>
   </div></div>
 <!--End-->

 <!--Second Container-->
 <div class="row">
   <div class="col-lg-12">
       <div class="panel panel-default">
         <div class="panel-body">
           <div id="view_listing">
             <table class="table table-striped table-bordered table-hover" width="2020">
               <tr>
                 <td><b>Sno</b></td>
                 <td>Created On</td>
                 <td>Area Category</td>
                 <td>Exact Area</td>
                 <td>Region</td>
                 <td>Locality</td>
                 <td>Details</td>
               </tr>
               <tr>
                 <td><b>1</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>2</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details"  type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>3</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>4</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>5</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>6</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>7</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
               <tr>
                 <td><b>8</b></td>
                 <td>Date</td>
                 <td>Commercial</td>
                 <td>10,000 sq ft</td>
                 <td>Sec-10</td>
                 <td>Chandigarh</td>
                 <td><button data-toggle="modal" data-target="#details" type="button" class="btn btn-success">Details</button></td>
               </tr>
             </table>

           </div>

         </div>
       </div>
     </div>


   </div>


<!--modal-->
<div class="modal fade" id="new-lstng">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:auto; overflow:auto">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">x</span>
          <span class="sr-only">Close</span>
          </button>
        <h5 class="modal-title">Add New Listing</h5>
      </div>
        <table width="100%" class="table table-striped table-bordered table-hover">
          <tr>
            <td>Client ID</td>
            <td><input class="form-control" id="client_id" value="manish kumar"></td>
          </tr>
          <tr>
            <td>Region</td>
            <td><select class="form-control" id="int-area">
              <option>Chandigarh</option>
              <option>Panchkula</option>
              <option>Mohali</option>
            </select></td>
          </tr>
          <tr>
            <td>Area Type</td>
            <td><select class="form-control" id="int-area">
              <option>Retail</option>
              <option>Warehouse</option>
              <option>Residential</option>
            </select></td>
          </tr>
          <tr>
            <td>Area Category</td>
            <td><select class="form-control" id="int-area">
              <option>100-500</option>
              <option>501-1000</option>
              <option>1001-2000</option>
              <option>2001-3000</option>
              <option>3001-5000</option>
              <option>5001-10000</option>
              <option>10001-20000</option>
              <option>20001-50000</option>
              <option>50000-above</option>
            </select></td>
          </tr>
          <tr>
            <td>Area Exact</td>
            <td><input class="form-control" id="client_id" value="10,000 sq ft"></td>
          </tr>
          <tr>
            <td>Availability Type</td>
            <td><select class="form-control" id="int-area">
              <option>Rent</option>
              <option>Lease</option>
            </select></td>
          </tr>
          <tr>
            <td></td>
            <td><button type="button" class="btn btn-success" style="margin-left:400px; width:100px">Add</button>
            </tr>
        </table>
      </div>
      </div>
    </div>
  </div>

<!--modal-->
    <div class="modal fade" id="details">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">
          <span aria-hidden="true">x</span>
          <span class="sr-only">Close</span>
          </button>
        <h5 class="modal-title">Listing Details</h5>
      </div>
      <div class="modal-body">
        <img src="img/gallery/imgbox1.jpg" class="rounded mx-auto d-block" alt="listing image">
        <table class="table" width="2020">
          <tr>
            <td><b>Location:</b></td>
            <td>Plot No 143-A, Purv Marg Industrial Area Phase 1 Chandigarh</td>
          </tr>
          <tr>
            <td><b>Description</b></td>
            <td>This is the place where we can add some descirption associated with this listing. This will be added at the time listing is posted.</td>
          </tr>
          <tr>
            <td><b>Carpet Area</b></td>
            <td>10,000 Sq Ft</td>
          </tr>
          <tr>
            <td><b>Price</b></td>
            <td>Rs24,000,000</td>
          </tr>
          <tr>
            <td><b>Availability Type</b></td>
            <td>For Lease</td>
          </tr>
          <tr>
            <td><b>Legal Disputes</b></td>
            <td>None</td>
          </tr>
          <tr>
            <td><b>Contact Person</b></td>
            <td>Mr Manish Kumar</td>
          </tr>
          <tr>
            <td><b>Contact Email</b></td>
            <td>abc@abc.com</td>
          </tr>
          <tr>
            <td><b>Contact Phone</b></td>
            <td>0000000000</td>
          </tr>
          <tr>
            <td><b>Website</b></td>
            <td>abc.com</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<!--end-modal-->


<script src="../js/excanvas.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.ui.custom.js"></script>
<script src="../js/jquery.flot.min.js"></script>
<script src="../js/jquery.flot.resize.min.js"></script>
<script src="../js/jquery.peity.min.js"></script>
<script src="../js/matrix.js"></script>
<script src="../js/matrix.chat.js"></script>
<script src="../js/jquery.wizard.js"></script>
<script src="../js/matrix.popover.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.flot.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://app.sliderui.com/sliders/qsokxm.js"></script>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/sb-admin-2.js"></script>
<script src="../js/listings.js"></script>
<script>
  function showListingDetailModal(){
    alert();
    $('#pop-up').modal('show');
  };
</script>

<script>
  function showListing(){
    alert();
    $('#details').modal('show');
  };
</script>
<script>
var myIndex = 0;
carousel();
function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 2500);
}
function showAddListing(){
  alert();
  $('#new-lstng').modal('show');
}

</script>
</body>
</html>
<!--End-->
