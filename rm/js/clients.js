$(document).ready(function() {
  fetchClients();
});


function doClientKeywordSearch(){

}



function fetchClients(){
  $(document.body).css({'cursor' : 'wait'});
  $('#clients_data').html('fetching data please wait...');
  $.ajax({
    url: "../endpoint/fetchClients.php",
    type: "POST",
    data: {
      result_mode:1
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
            $('#clients_data').html(parsedResponse.data);
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}

function toggleClient(actionCode, clientID){
  $(document.body).css({'cursor' : 'wait'});
  $('#action_status').html('updating status please wait...');
  $.ajax({
    url: "../endpoint/toggleClients.php",
    type: "POST",
    data: {
      action_mode:actionCode,
      client_id:clientID
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
            $('#action_status').html('');
            fetchClients();
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}


function editClient(clientID){
  var url = '../pages/client_details.php';
var form = $('<form action="' + url + '" method="post">' +
  '<input type="text" name="client_id" value="' + clientID + '" /></form>');
  $('body').append(form);
  form.submit();
}

function detailsClient(clientID){
  var url = '../pages/client_details.php';
  var form = $('<form action="' + url + '" method="post">' +
  '<input type="text" name="client_id" value="' + clientID + '" /></form>');
  $('body').append(form);
  form.submit();
}


function fetchClientDetails(){
  var clientID=$('#cid').html();
  $('#client_details').html('please wait..');
  $.ajax({
    url: "../endpoint/fetchClientDetails.php",
    type: "POST",
    data: {
      client_id:clientID
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
            $('#client_details').html(parsedResponse.data);
            fetchClients();
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}


function viewClientListings(){
  var clientID=$('#cid').html();
  var url = '../pages/view_listings.php';
  var form = $('<form action="' + url + '" method="post">' +
  '<input type="text" name="client_id" value="' + clientID + '" /></form>');
  $('body').append(form);
  form.submit();
}
function addClientListings(){
  var clientID=$('#cid').html();
  var url = '../pages/add_listing.php';
  var form = $('<form action="' + url + '" method="post">' +
  '<input type="text" name="client_id" value="' + clientID + '" /></form>');
  $('body').append(form);
  form.submit();
}

function updateClientData(){
  var clientID=$('#cid').html();
  var clientName=$('#client_name').val();
  if(clientName==""){
    alert('please provide client\'s name');
    return;
  }var clientAddress=$('#client_address').val();
  if(clientAddress==""){
    alert('please provide client\'s address');
    return;
  }var clientPhoneNumber=$('#client_mobile').val();
  if(clientPhoneNumber=="" || clientPhoneNumber.length!=10){
    alert('please provide a valid 10 digit mobile number');
    return;
  }
$(document.body).css({'cursor' : 'wait'});
  $('#status_code').html('please wait..');
  $.ajax({
    url: "../endpoint/updateClientData.php",
    type: "POST",
    data: {
      client_id:clientID,
      client_mobile_number:clientPhoneNumber,
      client_address:clientAddress,
      client_name:clientName
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
          $('#status_code').html('');
          alert(parsedResponse.response_message);
            fetchClientDetails();
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}
