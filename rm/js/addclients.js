function validateForm(){
  var client_name = $('#cname').val();
  if(client_name==="" || client_name==undefined){
    alert('please enter  client name');
    return false;
  }
  var client_mobile = $('#mobile').val();
  if(client_mobile==="" || client_mobile==undefined || client_mobile.length!=10){
    alert('please enter  client mobile');
    return false;
  }
  var client_address = $('#address').val();
  if(client_address==="" || client_address==undefined){
    alert('please enter  client address');
    return false;
  }
  var client_email = $('#email').val();
  if(client_email==="" || client_email==undefined){
    //alert('please enter  client email');
    //return false;
    $('[name=email]').val("--");
    return true;
  }
  }


function addClient(){
  $('#btnAddClient').html('Please wait...');
  $("#btnAddClient").prop('disabled', true);
  $(document.body).css({'cursor' : 'wait'});
  if(!validateForm()){
      return;
  }
  var client_name = $('#cname').val();
  var client_mobile = $('#mobile').val();
  var client_address = $('#address').val();
  var client_email = $('#email').val();
$.ajax({
  url: "../endpoint/addclient.php",
  type: "POST",
  data: {
    cname:client_name,
    mobile:client_mobile,
    address:client_address,
    email:client_email
},
    success: function(data){
      var parsedResponse = JSON.parse(data);
      if(parsedResponse.response_code==1){
          alert(parsedResponse.response_message);
          window.location.href="../pages/clients.php";
      }else{
          alert(parsedResponse.response_message);
          $('#btnAddClient').html('Add');
          $("#btnAddClient").prop('disabled', false);
      }
      $(document.body).css({'cursor' : 'default'});
    }
});

}
