<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Classifieds</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
       rel = "stylesheet">
    <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <!-- Javascript -->
    <script>
       $(function() {
          $( "#datepicker-13" ).datepicker();
          $( "#datepicker-13" ).datepicker("show");
       });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Updates App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                        </li>
						<li>
                            <a href="classifieds.php" class="active"><i class="fa fa-user fa-fw"></i> Classifieds</a>
                        </li>
						<li>
                            <a href="public_users.php"><i class="fa fa-user-secret fa-fw"></i> Public Users</a>
                        </li>
						<li>
                            <a href="clients.php"><i class="fa fa-pencil fa-fw"></i> Clients</a>
                        </li>
						<li>
                            <a href="sales.php"><i class="fa fa-bullhorn fa-fw"></i> Sales R.M.</a>
                        </li>
					</ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Classifieds</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="pull-right"><a type="button" class="btn btn-warning btn-lg" href="view.php">
              View Classified</a> </div>
            <div class="row">
                <div class="col-lg-12">
                    <br><div class="panel panel-default">
                        <div class="panel-heading">
                            Add New Add
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <br><br>
                            <div class="row">
                              <div class="col-lg-12">
                              <form class="form-inline">
                                    <label for="ex1">Enter Client number</label>
                                    <input class="form-control" id="ex1" type="text">
                              <a type="button" class="btn btn-info ">Generate OTP</a>
                            <label for="ex1">Enter OTP</label>
                              <input class="form-control" id="ex1" type="text">
                              <button type="button" class="btn btn-success ">Validate</button>
                            </form>
                        </div>

                              </div>


                            <br><br><div class ="col-lg-3">
                                <label for="ex1">Title</label>
                                <input class="form-control" id="ex1" type="text"><br>
                          </div>
                          <div class ="col-lg-3">
                            <label for="sel1">Category</label>
                            <select class="form-control" id="sel1">
                              <option>--Select--</option>
                              <option>Property</option>
                              <option>Flat</option>
                              <option>Plot</option>
                            </select>

                            <div class="checkbox">
                              <label><input type="checkbox" value="">Option 1</label>
                            </div>
                          <div class="checkbox">
                              <label><input type="checkbox" value="">Option 2</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" value="">Option 3</label>
                            </div>

                          </div>
                          <br><form class="form-inline">
                          <label for="ex1">From</label>
                          <input class="form-control" id="ex1" type="text">
                          <label for="ex12"> To</label>
                          <input class="form-control" id="ex2" type="text">
                        <a type="button" class="btn btn-info ">  Search</a>
                        </form>

                      </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Student Details</h4>
        </div>
        <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<p><b>Student Name:</b> XXXX</p>
				<p><b>Email:</b> XXXX@gmail.com</p>
				<p><b>Sign Up Date:</b> XX-XX-XXXX</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
			</div>
        </div>
      </div>

    </div>
</div>
<!-- Modal End -->


    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
