<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Public Users</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Updates App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="public_users.php"><i class="fa fa-dashboard fa-fw"></i>Back</a>
                        </li>
					                 </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Public Users Details</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="col-lg-8">
                        <h4>User Details</h4><hr>

                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Mobile number</th>
                                    <th>Email</th>
                                    <th> Bank account</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="odd gradeX">
                                    <td>ABC</td>
                                    <td>Male</td>
                                    <td>123456789</td>
                                    <td>ABC@gmail.com</td>
                                    <td >412345</td>
                                  </tr>
                                <tr class="even gradeC">
                                  <td>ABC</td>
                                  <td>Male</td>
                                  <td>123456789</td>
                                  <td>ABC@gmail.com</td>
                                  <td >412345</td></tr>
                                <tr class="odd gradeA">
                                  <td>ABC</td>
                                  <td>Male</td>
                                  <td>123456789</td>
                                  <td>ABC@gmail.com</td>
                                  <td >412345</td>  </tr>

                            </tbody>
                        </table>
                      </div>
                  <div class ="col-lg-4">
                      <br><br><br><img src="cinqueterre.jpg" class="img-rounded" align="right" alt="Cinque Terre" width="250" height="200">
                    </div>
                  </div>
                </div>
                </div>
              </div>


              <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                      <tr>
                          <th>Sno.</th>
                          <th>Query ID</th>
                          <th>Query Title</th>
                          <th>Query Description</th>
                          <th>Posted On</th>
                          <th>Details</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr class="odd gradeX">
                          <td>1</td>
                          <td>122</td>
                          <td>XXX</td>
                          <td>YYY</td>
                          <td >06-04-2017</td>
                          <td class="center"><button type="button" class="btn btn-primary" style="width:100%">Details</button></td>
                    </tr>
                      <tr class="even gradeC">
                        <td>1</td>
                        <td>122</td>
                        <td>XXX</td>
                        <td>YYY</td>
                        <td >06-04-2017</td>
                        <td class="center"><button type="button" class="btn btn-primary" style="width:100%">Details</button></td>
                    <tr class="odd gradeA">
                      <td>1</td>
                      <td>122</td>
                      <td>XXX</td>
                      <td>YYY</td>
                      <td >06-04-2017</td>
                      <td class="center"><button type="button" class="btn btn-primary" style="width:100%">Details</button></td>
                </tr>

                  </tbody>
              </table>


                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agent Details</h4>
        </div>
        <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<p><b>Agent Name:</b> XXXX</p>
				<p><b>Email:</b> XXXX@gmail.com</p>
				<p><b>Sign Up Date:</b> XX-XX-XXXX</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
			</div>
        </div>
      </div>
	  </div>

    </div>
</div>
<!-- Modal End -->

<!-- Modal -->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Agent</h4>
        </div>
        <div class="modal-body">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="form-group">
                        <label>Agent Name</label>
                        <input class="form-control" placeholder="Name">
					</div>
					<div class="form-group">
                        <label>Date of Birth</label>
                        <input class="form-control" placeholder="Date">
					</div>
					<div class="form-group">
                        <label>Email</label>
                        <input class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
                        <label>Password</label>
                        <input class="form-control" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-success">Create</button>
				</form>
			</div>
        </div>
      </div>
	  </div>

    </div>
</div>
<!-- Modal End -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
