<?php
session_start();
require '../endpoint/constants.php';//
if($_SESSION["stoken"]!=$authtoken){
  die('unauthorised access');
}
$clientID=$_POST['client_id'];
echo "<div id='cid'style='display:none'>".$clientID."</div>";
$clientName=$_POST['client_name'];
echo "<div id='cname'style='display:none'>".$clientName."</div>";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ADD Listings</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Updates App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="clients.php"><i class="fa fa-dashboard fa-fw"></i>Back</a>
                        </li>

					</ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add a new listing</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-lg-12">
                <div  class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div id="listing_form">
                        <form method="POST"
                        enctype="multipart/form-data"
                        action="../endpoint/listing_submission.php">
                        <table width="100%" class="table table-striped table-bordered table-hover">
                          <tr>
                            <td>Client ID</td>
                            <td><input class="form-control" name="client_id" id="client_id" value="" readonly></td>
                          </tr>
                          <tr>
                            <td>Client Name</td>
                            <td><input class="form-control" name="client_name" id="client_name" value="" readonly></td>
                          </tr>
                          <tr>
                            <td>Region</td>
                            <td><select name="listing_location" name="listing_location" class="form-control" id="listing_location">
                              <option value="1">Chandigarh</option>
                              <option value="2">Panchkula</option>
                              <option value="3">Mohali</option>
                              <option value="4">Zirakpur</option>
                              <option value="5">Kharar</option>
                              <option value="6">Dera Bassi</option>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Area Type</td>
                            <td><select class="form-control" name="listing_area_type">
                              <option value="1">Office Space</option>
                              <option value="2">Retail Space</option>
                              <option value="3">Warehouse</option>
                              <option value="4">Co Working</option>
                              <option value="5">Industrial Land/Plot</option>
                              <option value="6">Hotels</option>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td><input class="form-control" name="listing_address" value=""></td>
                          </tr>
                          <tr>
                            <td>Area Category</td>
                            <td><select class="form-control" name="area_category">
                              <option value="1">100-500</option>
                              <option value="2">501-1000</option>
                              <option value="3">1001-2000</option>
                              <option value="4">2001-3000</option>
                              <option value="5">3001-5000</option>
                              <option value="6">5001-10000</option>
                              <option value="7">10001-20000</option>
                              <option value="8">20001-50000</option>
                              <option value="9">50000-above</option>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Poperty Listing Type</td>
                            <td>
                              <select class="form-control" name="listing_posted_by">
                                <option value="1">BROKER</option>
                                <option value="2">OWNER</option>
                              </select>
                          </td>
                          </tr>
                          <tr>
                            <td>Area Exact</td>
                            <td><input class="form-control" name="exact_area" value=""></td>
                          </tr>
                          <tr>
                            <td> Price per Sq/Ft</td>
                            <td><input class="form-control" name="price" value=""></td>
                          </tr>
                          <tr>
                            <td>Contact Person</td>
                            <td><input class="form-control" name="contact_person" value=""></td>
                          </tr>
                          <tr>
                            <td>Contact Phone</td>
                            <td><input class="form-control" name="contact_phone" value=""></td>
                          </tr>
                          <tr>
                            <td>Contact Email</td>
                            <td><input class="form-control" name="contact_email" value=""></td>
                          </tr>
                          <tr>
                            <td>Contact Website</td>
                            <td><input class="form-control" name="contact_website" value=""></td>
                          </tr>
                          <tr>
                            <td>Availability Type</td>
                            <td><select class="form-control" name="availablilty_type">
                              <option>Lease</option>
                              <option>Sale</option>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Images</td>
                            <td><input type="file" name="img__[]" accept="image/x-png,image/gif,image/jpeg"  multiple></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <input class="btn btn-md btn-success" type="submit" ></input>

                            </tr>
                        </table>
                      </form>
                      </div>

                    </div>
                  </div>
                </div>


              </div>
              <div><br><br>



        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->




    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/listings.js"></script>
<script>
    $(document).ready(function() {
        preloadData();
    });
</script>
</body>

</html>
