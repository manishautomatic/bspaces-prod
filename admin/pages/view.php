<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>View Classifieds</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Updates App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

          <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="classifieds.php" class="active"><i class="fa fa-dashboard fa-fw"></i>Back</a>
                        </li>
						</ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 >View Classifieds</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <br><br><div class="row">
              <div class="col-lg-12">
                <div class="col-lg-4">
                      <label> Search by Keyword / Classified ID / Client ID </label>
                  <div class="input-group">
                    <input type="text" class="form-control" >
                    <div class="input-group-btn">
                      <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
              </div>

              <div class="col-lg-3">
                <label> Search by Posting date </label>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="DD-MMM-YYYY">
    <div class="input-group-btn">
      <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
    </div>
              </div>
            </div>

            </div><br><br><br><br>



            <div class="row">
                <div class="col-lg-12">
                    <div>

                        <!-- /.panel-heading -->
                        <br><div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S no.</th>
                                        <th>Posting Date</th>
                                        <th>Client Name</th>
                                        <th>Expires On</th>
                                        <th>Action</th>
                                        <th>ID</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>1</td>
                                        <td>01-03-2017</td>
                                        <td>ABC</td>
                                        <td>01-06-2017</td>
                                        <td class="center">
                                          <div class="btn-group" style="width:100%">
                                          <button type="button" class="btn btn-danger" style="width:50%">STOP</button>
                                        <button type="button" class="btn btn-success" style="width:50%">RENEW</button></div>
                                          </td>
                                      <td>80</td>
                                    </tr>
                                    <tr class="even gradeC">
                                      <td>1</td>
                                      <td>01-03-2017</td>
                                      <td>ABC</td>
                                      <td>01-06-2017</td>
                                      <td class="center">
                                        <div class="btn-group" style="width:100%">
                                        <button type="button" class="btn btn-danger" style="width:50%">STOP</button>
                                      <button type="button" class="btn btn-success" style="width:50%">RENEW</button></div>
                                      </td>
                                      <td>80</td>
                                      </tr>
                                    <tr class="odd gradeA">
                                      <td>1</td>
                                      <td>01-03-2017</td>
                                      <td>ABC</td>
                                      <td>01-06-2017</td>
                                      <td class="center">
                                        <div class="btn-group" style="width:100%">
                                        <button type="button" class="btn btn-danger" style="width:50%">STOP</button>
                                      <button type="button" class="btn btn-success" style="width:50%">RENEW</button></div>
                                      </td>
                                      <td>80</td>
                                    </tr>
                                    <tr class="even gradeA">
                                      <td>1</td>
                                      <td>01-03-2017</td>
                                      <td>ABC</td>
                                      <td>01-06-2017</td>
                                      <td class="center">
                                        <div class="btn-group" style="width:100%">
                                        <button type="button" class="btn btn-danger" style="width:50%">STOP</button>
                                      <button type="button" class="btn btn-success" style="width:50%">RENEW</button></div>
                                      </td>
                                      <td>80</td>
                                        </tr>


                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
