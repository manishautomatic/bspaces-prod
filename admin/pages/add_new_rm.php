<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add New R.M.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Updates App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="Sales.php"><i class="fa fa-dashboard fa-fw"></i>Back</a>
                        </li>

					</ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New R.M.</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-lg-12">
                <div  class="col-lg-5">
                  <div class="panel panel-default">
                    <div class="panel-body">
                        <label for="ex1">Name</label>
                      <input class="form-control" id="rmname" type="text"><br>
                      <label for="ex1">Mobile</label>
                      <input class="form-control" id="mobile" type="text"><br>
                      <label for="ex1">Email</label>
                      <input class="form-control" id="email" type="text"><br>
                      <label for="pasword">Password</label>
                      <input class="form-control" id="pass1" type="password"><br>
                      <label for="cpassword">Confirm Password</label>
                      <input class="form-control" id="pass2" type="password"><br>
                      <button type="submit"
                      class="btn btn-md btn-success"
                      id="btnaddRm" onclick="addrm()">Save</button>
                    </div>
                  </div>
                </div>
                  <div class="col-lg-1"></div>
              </div>
              <div><br><br>



        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- Modal -->

    </div>
</div>
<!-- Modal End -->
    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script scr="../js/add-rm.js"></script>
    <script scr="../js/md5.js"></script>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

    <script>
    function passwordvalidation(){
    var pass = document.getElementById("password").value;
    var pass = document.getElementById("cpassword").value;
    if (password != cpassword) {
     alert("Passwords Do not Match");
     return false;
   }
     else{
      return true;
   }

   if( password == "" || cpassword == ""){
     alert ("Enter both fields");
     return false;
   }
   return true;
 }
    </script>
    <script>
    function validateForm(){
      var rm_name = $('#rmname').val();
      if(rm_name==="" || rm_name==undefined){
        alert('please enter RM name');
        return false;
      }
      var rm_mobile = $('#mobile').val();
      if(rm_mobile==="" || rm_mobile==undefined || rm_mobile.length!=10){
        alert('please enter  rm mobile number');
        return false;
      }

      var rm_email = $('#email').val();
      if(rm_email==="" || rm_email==undefined){
        //alert('please enter  client email');
        //return false;
        $('[name=email]').val("--");
        return true;
      }
      var rm_password = $('#pass1').val();
      if(rm_password=="" || rm_password==undefined){
        alert ('please enter a valid password');
        return false;
      }
      var rm_cpassword= $('#pass2').val();
      if(rm_cpassword===""|| rm_password!=rm_password){
        alert('passwords are different');
        return false;
      }
      return true;
      }
      function addrm(){
      if(!validateForm()){
          return;
      }
      $('#btnaddrm').html('Please wait...');
      $("#btnaddrm").prop('disabled', true);
      $(document.body).css({'cursor' : 'wait'});
      var rm_name = $('#rmname').val();
      var rm_mobile = $('#mobile').val();
      var rm_email = $('#email').val();
      var rm_password = $('#pass1').val();

    $.ajax({
      url: "../endpoint/add-rm.php",
      type: "POST",
      data: {
        rmname:rm_name,
        mobile:rm_mobile,
        email:rm_email,
        password:rm_password
    },
        success: function(data){
          var parsedResponse = JSON.parse(data);
          if(parsedResponse.response_code==1){
              alert(parsedResponse.response_message);
              window.location.href="../pages/sales.php";
          }
          else{
              alert(parsedResponse.response_message);
              $('#btnaddrm').html('Add');
              $("#btnaddrm").prop('disabled', false);
          }
          $(document.body).css({'cursor' : 'default'});
        }
    });
    }
    </script>

</body>

</html>
