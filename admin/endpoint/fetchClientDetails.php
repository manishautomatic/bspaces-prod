<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$clientID="";

if(!isset($_POST['client_id'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing client id');
  die(json_encode($responseArray));
}
$clientID=$_POST['client_id'];




$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }


  // first we check if the mobile number is already taken or not...

  $getClientDetails = "select * from users where type=1 and user_id='$clientID' ";
  $result= mysqli_query($con,$getClientDetails);
  if($result){
    // now format the table and return it to the page...
    $formattedData='<table width="100%"
                           class="table table-striped table-bordered table-hover"
                           id="dataTables-athletes" ><tbody>';

      while($row=mysqli_fetch_array($result)){
        $statusText="";
        if($row['status']=="1"){
          $statusText='Live';
        }else{
          $statusText='Disabled';
        }
        $formattedData=$formattedData
                      .'<tr>
                        <td>ID: </td>
                        <td>'.$row['user_id'].'</td>
                       </tr><tr>
                        <td>Name: </td>
                        <td><input class="form-control" id="client_name" value="'.$row['name'].'"></input></td>
                       </tr><tr>
                         <td>Mobile Number: </td>
                         <td><input class="form-control" id="client_mobile" value="'.$row['mobile'].'"></input></td>
                        </tr><tr>
                          <td>SignUp Date: </td>
                          <td>'.$row['created_on'].'</td>
                        </tr><tr>
                           <td>Address: </td>
                           <td><input class="form-control" id="client_address" value="'.$row['address'].'"></input></td>
                        </tr><tr>
                          <td>Status: </td>
                          <td>'.$statusText.'</td>
                        </tr>';
      }
      // now we find the numbers of listings for this client
      $fetchtotalListingsCount="select COUNT(*) AS `count` from listings
                                where owner_id='$clientID' ";
      $result = mysqli_query($con,$fetchtotalListingsCount);
      $listingCount=-1;
      if($result){
        while($row=mysqli_fetch_array($result)){
              $listingCount=$row['count'];
        }
      }
      $formattedData=$formattedData.'<tr>
                                          <td>Listings: </td>
                                          <td>'.$listingCount.'</td>
                                    </tr>
                                    <tr>
                                          <td>View Listings: </td>
                                          <td><button class="btn btn-md btn-primary" onclick="viewClientListings()">VIEW LISTINGS</button></td>
                                    </tr>
                                    <tr>
                                          <td>Add Listings: </td>
                                          <td><button class="btn btn-md btn-success" onclick="addClientListings()">ADD LISTING </button></td>
                                    </tr>
                                    <tr>
                                          <td>Update Data: </td>
                                          <td><button class="btn btn-md btn-warning" onclick="updateClientData()">UPDATE DATA</button></td>
                                    </tr>
                                    </tbody></table>';
      $responseArray = array('response_code'=>1,
                             'response_message'=>'data fetch ok',
                             'data'=>$formattedData);


      //$responseArray = array('response_code'=>1,'response_message'=>'');
      die(json_encode($responseArray));
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'db I/O error 55');
    die(json_encode($responseArray));
  }






 ?>
