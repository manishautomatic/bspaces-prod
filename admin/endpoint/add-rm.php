<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$rm_name="";
$rm_mobile="";
$rm_email="";
$rm_password="";

if(!isset($_POST['rmname'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing rm name');
  die(json_encode($responseArray));
}
if(!isset($_POST['mobile'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing rm mobile');
  die(json_encode($responseArray));
}

if(!isset($_POST['email'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing rm email');
  die(json_encode($responseArray));
}
if(!isset($_POST['password'])){
  $responseArray = array('response_code'=>0, 'response_message'=>'missing rm pass');
  die(json_encode($responseArray));
}

$rm_name=$_POST['rmname'];
$rm_mobile=$_POST['mobile'];
$rm_email=$_POST['email'];
$rm_password=$_POST['password'];



$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }

  // first we check if the mobile number is already taken or not...
  $validateUniqueMobile = "select mobile from users where mobile= '$rm_mobile'";
  //die(print_r($validateUniqueMobile));
  $result= mysqli_query($con,$validateUniqueMobile);
  if($result){
      while($row=mysqli_fetch_array($result)){
          if($rm_mobile==$row['mobile']){
            $responseArray = array('response_code'=>0,'response_message'=>'this mobile number is already taekn');
            die(json_encode($responseArray));
          }
      }
  }else
  {

    die(var_dump($result));
    $responseArray = array('response_code'=>0,'response_message'=>'db I/O error 55');
    die(json_encode($responseArray));
  }

  $created_date = date("Y-m-d H:i:s");
  $hash = hexdec(sha1($authtoken.$rm_mobile));
  $trimmedhash = substr($hash, 0, 8);
  $trimmedhash=str_replace('.','',$trimmedhash);
  $accessToken = md5($authtoken.$rm_mobile.$created_date);

  $insertNewRMQuery = "insert into users (mobile, type,token, user_id,name,password)
                            values ('$rm_mobile',
                                    '2',
                                    '$accessToken',
                                    '$trimmedhash',
                                    '$rm_name',
                                    '$rm_password')";

    //die($insertNewRMQuery);
  $result = mysqli_query($con,$insertNewRMQuery);
  if($result){
    $responseArray = array('response_code'=>1,'response_message'=>' added successfully.');
    die(json_encode($responseArray));
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'could not create user 82');
    die(json_encode($responseArray));
  }

 ?>
