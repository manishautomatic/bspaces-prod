<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }





$clientID="";
$clientName="";
$listingRegion="";
$listingAreaType="";
$listingAreaCategory="";
$listingAddress="";
$listingAreaType="";
$listingAreaExact="";
$listingPricePerSqFt="";
$listingContactPersonName="";
$listingContactPersonPhone="";
$listingContactPersonEmail="";
$listingContactPersonWebsite="";
$listingType="";
$listingAvailabilityType="";
$listingPostedBy="";

if(isset($_POST['client_id'])){
$clientID=$_POST['client_id'];
}
if(isset($_POST['client_name'])){
$clientName=$_POST['client_name'];
}
if(isset($_POST['listing_location'])){
$listingRegion=$_POST['listing_location'];
}
if(isset($_POST['listing_area_type'])){
$listingAreaType=$_POST['listing_area_type'];
}
if(isset($_POST['listing_address'])){
$listingAddress=$_POST['listing_address'];
}
if(isset($_POST['area_category'])){
$listingAreaCategory=$_POST['area_category'];
}
if(isset($_POST['listing_type'])){
$listingType=$_POST['listing_type'];
}
if(isset($_POST['exact_area'])){
$listingAreaExact=$_POST['exact_area'];
}if(isset($_POST['price'])){
$listingPricePerSqFt=$_POST['price'];
}
if(isset($_POST['contact_person'])){
$listingContactPersonName=$_POST['contact_person'];
}
if(isset($_POST['contact_phone'])){
$listingContactPersonPhone=$_POST['contact_phone'];
}
if(isset($_POST['contact_email'])){
$listingContactPersonEmail=$_POST['contact_email'];
}if(isset($_POST['contact_website'])){
$listingContactPersonWebsite=$_POST['contact_website'];
}
if(isset($_POST['availablilty_type'])){
$listingAvailabilityType=$_POST['availablilty_type'];
}//listing_posted_by
if(isset($_POST['listing_posted_by'])){
$listingPostedBy=$_POST['listing_posted_by'];
}


// now we generate the listingID
$listingID="";
$created_date = date("Y-m-d H:i:s");
$hash = hexdec(sha1($authtoken.$clientID.$created_date));
$trimmedhash = substr($hash, 0, 9);
$listingID=str_replace('.','',$trimmedhash);

// now we insert the listing record in the database

$insertListingQuery = "insert into listings (listing_id,
                                              owner_id,
                                              region,
                                              address,
                                              description,
                                              area,
                                              area_category,
                                              area_type,
                                              availability_type,
                                              disputes,
                                              contact_person,
                                              contact_email,
                                              contact_phone,
                                              website,
                                              published_by)
                        VALUES(
                            '$listingID',
                            '$clientID',
                            '$listingRegion',
                            '$listingAddress',
                            '',
                            '$listingAreaExact',
                            '$listingAreaCategory',
                            '$listingAreaType',
                            '$listingAvailabilityType',
                            '',
                            '$listingContactPersonName',
                            '$listingContactPersonEmail',
                            '$listingContactPersonPhone',
                            '$listingContactPersonWebsite',
                            '$listingPostedBy')";

    $result=mysqli_query($con,$insertListingQuery);
    if($result ){
      //echo "listing posted successfully";
      // now we check if any photos are submitted with this listing....
      $total = count($_FILES['img__']['name']);
      for($i=0; $i<$total; $i++) {
        $tmpFilePath = $_FILES['img__']['tmp_name'][$i];
        if ($tmpFilePath != ""){
      	$temp = explode(".", $_FILES["img__"]["name"][$i]);
      	$newfilename = microtime(true) . '.' . end($temp);
           $newFilePath = "../listing_pics/" . $newfilename;
           usleep(25000);
          if(move_uploaded_file($tmpFilePath, $newFilePath)) {
            //Handle other code here
            // this file has been uploaded successfully
            /// now insert its record in the database.
            $Query = "insert into listing_photo (listing_id,
            photo_path,enabled ) VALUES ('$listingID',
            																					'$newFilePath',
            																					'1') ";
      	$result=mysqli_query($con,$Query);
      	if($result){
      	}
      	else{

      	}

          }
        }
      }
      echo ''.'<script> function redirect(){alert(" Listing added successfully");
                  window.location = "../pages/client_details.php";}  </script>'
      			.'<body onload="redirect()"></body>';
      			echo $utcome;;
    } else{
      echo "could not post your listing.";
    }




 ?>
