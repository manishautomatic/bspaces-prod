<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$resultMode="";

if(!isset($_POST['result_mode'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing result mode');
  die(json_encode($responseArray));
}

$resultMode=$_POST['result_mode'];


$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }


  // first we check if the mobile number is already taken or not...

  $validateUniqueMobile = "select * from users where type=2 ";
  $result= mysqli_query($con,$validateUniqueMobile);
  if($result){
    // now format the table and return it to the page...
    $formattedData='<table width="100%"
                           class="table table-striped table-bordered table-hover"
                           id="dataTables-athletes" >
                    <thead> <tr>
                                <td>S.No.</td>
                                <td>Name</td>
                                <td>Sign Up Date</td>
                                <td>Mobile Number</td>
                                <td>Action</td>
                                <td>Edit</td>
                                <td>Details</td>
                      </tr></thead>
                    <tbody>';

        $clientDataArray=array();
        $counter=0;
      while($row=mysqli_fetch_array($result)){
        if($resultMode=="1"){
          $counter=$counter+1;
          $actionPacket="";
          if($row['status']==1){// the user is live and can be disabled
              $actionPacket='<button class="btn btn-md btn-danger" onclick="toggleRM(\'0\',\''.$row['user_id'].'\')">DISABLE</button>';
          }else{
              $actionPacket='<button class="btn btn-md btn-primary" onclick="toggleRM(\'1\',\''.$row['user_id'].'\')">ENABLE</button>';
          }
          $editPacket = '<button class="btn btn-md btn-primary" onclick="editRM(\''.$row['user_id'].'\')">EDIT</button>';
          $detailsPacket = '<button class="btn btn-md btn-primary" onclick="detailsRM(\''.$row['user_id'].'\')">DETAILS</button>';
          $formattedData=$formattedData
                         .'<tr>
                           <td>'.$counter.'</td>
                           <td>'.$row['name'].'</td>
                           <td>'.$row['created_on'].'</td>
                           <td>'.$row['mobile'].'</td>
                           <td>'.$actionPacket.'</td>
                           <td>'.$editPacket.'</td>
                           <td>'.$detailsPacket.'</td>
                         </tr>';

        }else{
          $clientDataArray[]=array('id'=>$row['user_id'],
                                   'name'=>$row['name'],
                                   'created_on'=>$row['created_on'],
                                   'mobile'=>$row['mobile']);
        }
      }
      if($resultMode=="1"){
        $formattedData=$formattedData.'</tbody></table>';
        $responseArray = array('response_code'=>1,
                               'response_message'=>'data fetch ok',
                               'data'=>$formattedData);
      }else{
        $responseArray = array('response_code'=>1,
                               'response_message'=>'data fetch ok',
                               'data'=>$clientDataArray);
      }

      //$responseArray = array('response_code'=>1,'response_message'=>'');
      die(json_encode($responseArray));
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'db I/O error 55');
    die(json_encode($responseArray));
  }






 ?>
