<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$client_name="";
$client_mobile="";
$client_address="";
$client_email="";
$client_type="";

if(!isset($_POST['cname'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing client name');
  die(json_encode($responseArray));
}
if(!isset($_POST['mobile'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing client mobile');
  die(json_encode($responseArray));
}
if(!isset($_POST['address'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing client address');
  die(json_encode($responseArray));
}
if(!isset($_POST['email'])){
//  $responseArray = array('response_code'=>0,'response_message'=>'missing client email');
  //die(json_encode($responseArray));
}else{
  $client_email=$_POST['email'];
}

$client_name=$_POST['cname'];
$client_mobile=$_POST['mobile'];
$client_address=$_POST['address'];

$client_type=$_POST['clienttype'];


$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }


  // first we check if the mobile number is already taken or not...

  $validateUniqueMobile = "select mobile from users where type=1 and mobile= '$client_mobile'";
  $result= mysqli_query($con,$validateUniqueMobile);
  if($result){
      while($row=mysqli_fetch_array($result)){
          if($client_mobile==$row['mobile']){
            $responseArray = array('response_code'=>0,'response_message'=>'this mobile number is already taekn');
            die(json_encode($responseArray));
          }
      }
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'db I/O error 55');
    die(json_encode($responseArray));
  }

  // now insert the client into the database.
  $created_date = date("Y-m-d H:i:s");
  $hash = hexdec(sha1($authtoken.$client_mobile));
  $trimmedhash = substr($hash, 0, 8);
  $trimmedhash=str_replace('.','',$trimmedhash);
  $accessToken = md5($authtoken.$client_mobile.$created_date);

  $insertNewClientQuery = "insert into users (mobile, type,token, user_id,name,address,client_type)
                            values ('$client_mobile',
                                    '1',
                                    '$accessToken',
                                    '$trimmedhash',
                                    '$client_name',
                                    '$client_address','$client_type')";
  $result = mysqli_query($con,$insertNewClientQuery);
  if($result){
    $responseArray = array('response_code'=>1,'response_message'=>'client added successfully.');
    die(json_encode($responseArray));
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'could not create user 82');
    die(json_encode($responseArray));
  }





 ?>
