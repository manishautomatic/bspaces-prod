<?php
require 'constants.php';//
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if($_SESSION["stoken"]!=$authtoken){
  $responseArray = array('response_code'=>0,'response_message'=>'Session expired, please logout and login again');
  die(json_encode($responseArray));
}

$actionMode="";
$clientID="";

if(!isset($_POST['action_mode'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing action mode.');
  die(json_encode($responseArray));
}
if(!isset($_POST['client_id'])){
  $responseArray = array('response_code'=>0,'response_message'=>'missing client id.');
  die(json_encode($responseArray));
}
$actionMode=$_POST['action_mode'];
$clientID=$_POST['client_id'];

$con=mysqli_connect($db_server,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()){
  $responseArray = array('response_code'=>0,'response_message'=>'db I/O error');
  die(json_encode($responseArray));
  }else{
  	//echo 'connection successfull<br>';
  }
  $query="";
  if($actionMode=="0"){// the admin wants to disable this client.
    $query ="update users set status=0 where type=2 and user_id='$clientID'";
  }else{// the admin wants to enable this client.
    $query ="update users set status=1 where type=2 and user_id='$clientID'";
  }

  $result = mysqli_query($con,$query);
  if($result){
    $responseArray = array('response_code'=>1,'response_message'=>'status updated successfully');
    die(json_encode($responseArray));
  }else{
    $responseArray = array('response_code'=>0,'response_message'=>'could not update status, code: 45');
    die(json_encode($responseArray));
  }

 ?>
