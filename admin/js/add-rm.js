function validateForm(){
  var rm_name = $('#rmname').val();
  if(rm_name==="" || rm_name==undefined){
    alert('please enter RM name');
    return false;
  }
  var rm_mobile = $('#mobile').val();
  if(rm_mobile==="" || rm_mobile==undefined || rm_mobile.length!=10){
    alert('please enter  rm mobile number');
    return false;
  }

  var rm_email = $('#email').val();
  if(rm_email==="" || rm_email==undefined){
    //alert('please enter  client email');
    //return false;
    $('[name=email]').val("--");
    return true;
  }
  var rm_password = $('#pass1').val();
  if(rm_password==="" || rm_password==undefined){
    alert ('please enter a valid password');
    return false;
  }
  var rm_cpassword= $('#pass2').val();
  if(rm_cpassword===""|| rm_password!=rm_password){
    alert('passwords are different');
    return false;
  }
  }
  function addrm(){
  if(!validateForm()){
      return;
  }
  $('#btnAddRm').html('Please wait...');
  $("#btnAddRm").prop('disabled', true);
  $(document.body).css({'cursor' : 'wait'});
  var rm_name = $('#rmname').val();
  var rm_mobile = $('#mobile').val();
  var rm_email = $('#email').val();
  var rm_password_crypt=md5(rm_password);

$.ajax({
  url: "../endpoint/add-rm.php",
  type: "POST",
  data: {
    rmname:rm_name,
    mobile:rm_mobile,
    email:rm_email
    password:rm_password_crypt
},
    success: function(data){
      var parsedResponse = JSON.parse(data);
      if(parsedResponse.response_code==1){
          alert(parsedResponse.response_message);
          window.location.href="../pages/sales.php";
      }else{
          alert(parsedResponse.response_message);
          $('#btnAddRm').html('Add');
          $("#btnAddRm").prop('disabled', false);
      }
      $(document.body).css({'cursor' : 'default'});
    }
});
}
