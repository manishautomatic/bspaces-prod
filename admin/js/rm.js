$(document).ready(function() {
  fetchRMs();
});

function fetchRMs(){
  $(document.body).css({'cursor' : 'wait'});
  $('#rm_data').html('fetching data please wait...');
  $.ajax({
    url: "../endpoint/fetchRMs.php",
    type: "POST",
    data: {
      result_mode:1
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
            $('#rm_data').html(parsedResponse.data);
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}


function toggleRM(actionCode, clientID){
  $(document.body).css({'cursor' : 'wait'});
  $('#action_status').html('updating status please wait...');
  $.ajax({
    url: "../endpoint/toggleRMs.php",
    type: "POST",
    data: {
      action_mode:actionCode,
      client_id:clientID
  },
      success: function(data){
        var parsedResponse = JSON.parse(data);
        if(parsedResponse.response_code==1){
            $('#action_status').html('');
            fetchRMs();
        }else{
            alert(parsedResponse.response_message);
        }
        $(document.body).css({'cursor' : 'default'});
      }
  });
}


function editRM(clientID){
  var url = '../pages/edit_rm.php';
var form = $('<form action="' + url + '" method="post">' +
  '<input type="text" name="client_id" value="' + clientID + '" /></form>');
  $('body').append(form);
  form.submit();
}
